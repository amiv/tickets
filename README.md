# AMIV Issue Tracking Hub

Welcome to the AMIV Issue Tracking Hub, the central repository for managing and tracking issues across various AMIV projects.

## Overview

This GitLab repository is dedicated exclusively to issue tracking, goal setting, and planning for AMIV's diverse range of projects. 
It utilizes GitLab's powerful issue tracking system to manage tasks, bugs, and notices efficiently.

## How It Works

- **Issues**: This is where all tasks, bugs, and notices are reported and tracked.
- **Milestones**: We use GitLab milestones to track the progress and plan for the various projects under the AMIV umbrella.

## Getting Started

To get started with reporting an issue or tracking a project's milestone:

1. **Create an Issue**: Click on the `Issues` tab and then `New Issue` to report a new task, bug, or notice.
2. **Assign Labels**: Use labels to categorize the issues for easy tracking and sorting.
3. **Set Milestones**: When creating an issue, you can assign it to a milestone for better planning and tracking of project goals.

## Contribution Guidelines

- **Search Existing Issues**: Before submitting a new issue, please search the existing issues to avoid duplicates.
- **Detailed Descriptions**: Provide as much detail as possible when creating an issue to facilitate quicker resolutions.
- **Follow the Template**: Use the provided issue templates for consistency and completeness.

## Milestone Tracking

- **View Milestones**: Access the `Milestones` tab to see all current and upcoming milestones.
- **Milestone Details**: Click on a milestone to view its description, deadline, and associated issues.

## Support

If you need assistance or have any questions regarding the issue tracking process, please reach out to the AMIV IT team at it@amiv.ethz.ch.

## Feedback

Your feedback is crucial to improve the Issue Tracking Hub. Please share your suggestions or comments by creating a `Feedback` issue with the corresponding label.

## Stay Updated

Keep an eye on the `Announcements` issue label for updates on the tracking process and milestones.

Thank you for contributing to the AMIV project's success!

